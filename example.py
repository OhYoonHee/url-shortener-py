from url_short import URL_SHORT
import asyncio

Shortener = URL_SHORT()

# example tinyurl
loop = asyncio.get_event_loop()
result = loop.run_until_complete(Shortener.tinyurl('https://gitlab.com'))
print(result)
