# Created by Oh_Yoon_Hee (https://github.com/OhYoonHee)
# Created at 2021 - 11 - 10 (years - month - day)
# License MIT

from aiohttp import ClientSession, FormData
from http.cookies import SimpleCookie
from urllib.parse import unquote
import re, time

_default_headers = {"Accept": "*/*", "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}

# https://stackoverflow.com/a/7160778
def valid_url(input: str) -> bool:
    regex = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return regex.match(input) is not None

class URL_SHORT():
    def __init__(self) -> None:
        pass

    async def tinyurl(self, url: str) -> dict:
        if valid_url(url) is False:
            return dict(status=False, error="Link yang anda berikan tidak valid")
        api = 'https://tinyurl.com/app/api/create'
        hasil = dict()
        request = ClientSession()
        try:
            headers = _default_headers
            headers.update({'x-requested-with': 'XMLHttpRequest'})
            test = await request.get('https://tinyurl.com/app', headers=headers)
            cookie = test.headers.get('Set-Cookie')
            sc = SimpleCookie()
            sc.load(cookie)
            headers.update({'x-xsrf-token': unquote(sc.get('XSRF-TOKEN').value)})
            json_request = dict(url=url, domain="tinyurl.com", alias="", tags=[], errors=dict(errors={}, busy=True, successful=True))
            request_api = await request.post(api, json=json_request, headers=headers)
            res = await request_api.json()
            data = res.get('data', [])
            if len(data) > 0:
                hasil.update(status=True, result=data[0])
            elif bool(res.get('errors', False)):
                hasil.update(status=False, error=res.get('message', 'Telah terjadi error yang tidak diketahui'))
        except:
            hasil.update(status=False, error="Telah terjadi error yang tidak diketahui")
        finally:
            await request.close()
            return hasil

    async def bitly(self, url: str) -> dict:
        if valid_url(url) is False:
            return dict(status=False, error="Link yang anda berikan tidak valid")
        api = "https://bitly.com/data/anon_shorten"
        hasil = dict()
        request = ClientSession()
        try:
            timestamp = time.time()
            modified = time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(timestamp))
            headers = _default_headers
            headers.update({ 'if-modified-since': modified })
            test = await request.get('https://bitly.com/', headers=headers)
            cookie = test.headers.get('Set-Cookie')
            sc = SimpleCookie()
            sc.load(cookie)
            headers.update({'x-xsrftoken': unquote(sc.get('_xsrf').value)})
            form = FormData()
            form.add_field('url', url)
            headers.update({"Accept": "*/*", 'x-requested-with': 'XMLHttpRequest'})
            request_api = await request.post(api, data=form, headers=headers)
            res = await request_api.json()
            status_code = res['status_code']
            if status_code == 200:
                hasil.update(status=True, result=res['data'])
            else:
                hasil.update(status=False, error=res['status_txt'])
        except:
            hasil.update(status=False, error="Telah terjadi error yang tidak diketahui")
        finally:
            await request.close()
            return hasil

    async def cuttly(self, url: str) -> dict:
        if valid_url(url) is False:
            return dict(status=False, error="Link yang anda berikan tidak valid")
        request = ClientSession()
        hasil = dict()
        try:
            api = "https://cutt.ly/scripts/shortenUrl.php"
            form = FormData((('url', url), ('domain', '1')))
            req = await request.post(api, data=form)
            res = await req.text()
            valid = valid_url(res)
            if valid:
                hasil.update(status=True, result=dict(url=res))
            else:
                hasil.update(status=False, error=res)
        except:
            hasil.update(status=False, error="Telah terjadi error yang tidak diketahui")
        finally:
            await request.close()
            return hasil

    async def sid(self, url: str):
        if valid_url(url) is False:
            return dict(status=False, error="Link yang anda berikan tidak valid")
        request = ClientSession()
        hasil = dict()
        try:
            api = 'https://home.s.id/api/public/link/shorten'
            form = FormData()
            form.add_field('url', url)
            req = await request.post(api, data=form, ssl=False)
            res = await req.json()
            if res.get('errors', None) is not None:
                hasil.update(status=False, error=res['errors'][0])
            else:
                hasil.update(status=True, result=res)
        except:
            hasil.update(status=False, error="Telah terjadi error yang tidak diketahui")
        finally:
            await request.close()
            return hasil
